use r3bl_ansi_color::{AnsiStyledText, Color, Style as RStyle};
use r3bl_tuify::*;
use rust_fuzzy_search::fuzzy_search_best_n;
use serde::{Deserialize, Serialize};
use std::process::{Command, Stdio};
use std::{env, fmt::Display};

#[derive(Serialize, Deserialize, Debug)]
struct Package {
    name: String,
    version: String,
    revision: u32,
    filename_size: u32,
    repository: String,
    short_desc: String,
}

impl AsRef<str> for Package {
    fn as_ref(&self) -> &str {
        &self.name.as_str()
    }
}
impl Display for Package {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }
}

#[derive(Serialize, Deserialize)]
struct Packages {
    data: Vec<Package>,
}

fn multi_select_instructions() -> Vec<Vec<AnsiStyledText<'static>>> {
    let up_and_down = AnsiStyledText {
        text: " Up or Down:",
        style: &[
            RStyle::Foreground(Color::Rgb(168, 153, 132)),
            RStyle::Background(Color::Rgb(40, 40, 40)),
        ],
    };
    let navigate = AnsiStyledText {
        text: "     navigate",
        style: &[
            RStyle::Foreground(Color::Rgb(213, 196, 161)),
            RStyle::Background(Color::Rgb(40, 40, 40)),
        ],
    };

    let line_1 = vec![up_and_down, navigate];

    let space = AnsiStyledText {
        text: " Space:",
        style: &[
            RStyle::Foreground(Color::Rgb(215, 153, 33)),
            RStyle::Background(Color::Rgb(40, 40, 40)),
        ],
    };
    let select = AnsiStyledText {
        text: "          select or deselect item",
        style: &[
            RStyle::Foreground(Color::Rgb(213, 196, 161)),
            RStyle::Background(Color::Rgb(40, 40, 40)),
        ],
    };

    let line_2 = vec![space, select];

    let esc = AnsiStyledText {
        text: " Esc or Ctrl+C:",
        style: &[
            RStyle::Foreground(Color::Rgb(177, 98, 134)),
            RStyle::Background(Color::Rgb(40, 40, 40)),
        ],
    };
    let exit = AnsiStyledText {
        text: "  exit program",
        style: &[
            RStyle::Foreground(Color::Rgb(213, 196, 161)),
            RStyle::Background(Color::Rgb(40, 40, 40)),
        ],
    };

    let line_3 = vec![esc, exit];
    let return_key = AnsiStyledText {
        text: " Return:",
        style: &[
            RStyle::Foreground(Color::Rgb(152, 151, 26)),
            RStyle::Background(Color::Rgb(40, 40, 40)),
        ],
    };
    let confirm = AnsiStyledText {
        text: "         confirm selection",
        style: &[
            RStyle::Foreground(Color::Rgb(213, 196, 161)),
            RStyle::Background(Color::Rgb(40, 40, 40)),
        ],
    };
    let line_4 = vec![return_key, confirm];
    vec![line_1, line_2, line_3, line_4]
}

pub fn custom_style() -> StyleSheet {
    let focused_and_selected_style = Style {
        fg_color: Color::Rgb(251, 73, 52),
        bg_color: Color::Rgb(40, 40, 40),
        ..Style::default()
    };
    let focused_style = Style {
        fg_color: Color::Rgb(235, 219, 178),
        bg_color: Color::Rgb(40, 40, 40),
        ..Style::default()
    };
    let unselected_style = Style {
        fg_color: Color::Rgb(189, 174, 147),
        bg_color: Color::Rgb(40, 40, 40),
        ..Style::default()
    };
    let selected_style = Style {
        fg_color: Color::Rgb(204, 36, 29),
        bg_color: Color::Rgb(40, 40, 40),
        ..Style::default()
    };
    let header_style = Style {
        fg_color: Color::Rgb(254, 128, 25),
        bg_color: Color::Rgb(40, 40, 40),
        ..Style::default()
    };
    StyleSheet {
        focused_and_selected_style,
        focused_style,
        unselected_style,
        selected_style,
        header_style,
    }
}

async fn search(search: &str) -> Result<(), Box<dyn std::error::Error>> {
    let client = reqwest::Client::new();
    let request = client.get("https://xq-api.voidlinux.org/v1/query/x86_64?q=");
    let Ok(built) = request.build() else {
        return Ok(());
    };
    let mut resp = client
        .execute(built)
        .await
        .unwrap()
        .json::<Packages>()
        .await
        .unwrap();
    let ls = String::from_utf8(
        Command::new("ls")
            .arg("/etc/xbps.d/")
            .output()
            .unwrap()
            .stdout,
    )
    .unwrap();
    let repos: Vec<&str> = ls
        .lines()
        .filter_map(|line| {
            if line.get(0..2) == Some("00") || line.get(0..2) == Some("10") {
                let repo = line.get(14..line.len() - 5);
                if repo == Some("main") {
                    Some("current")
                } else {
                    repo
                }
            } else {
                None
            }
        })
        .collect();
    resp.data.retain(|p| repos.contains(&p.repository.as_str()));
    let packages = fuzzy_search_best_n(&search, resp.data.as_slice(), 200);
    let max_height_row_count: usize = 8;

    let header = AnsiStyledText {
        text: " Select Packages To Install",
        style: &[
            RStyle::Foreground(Color::Rgb(214, 93, 14)),
            RStyle::Background(Color::Rgb(40, 40, 40)),
        ],
    };

    let mut instructions_and_header: Vec<Vec<AnsiStyledText>> = multi_select_instructions();
    instructions_and_header.push(vec![header]);

    let user_input = select_from_list_with_multi_line_header(
        instructions_and_header,
        packages.iter().map(|pckg| pckg.0.to_string()).collect(),
        Some(max_height_row_count),
        None,
        SelectionMode::Multiple,
        custom_style(),
    );

    match &user_input {
        Some(it) => {
            Command::new("xbps-install")
                .args(it)
                .status()
                .expect("Failed to install packages");
        }
        None => println!("There is nothing to be done"),
    }
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = &env::args().collect::<Vec<String>>();
    if args.len() > 1 {
        match args[1].as_str() {
            "search" | "se" => {
                elevate::escalate_if_needed()?;
                if args.len() > 2 {
                    search(&args[2]).await?;
                } else {
                    search("").await?;
                }
            }
            "install" | "in" => {
                elevate::escalate_if_needed()?;
                if args.len() > 2 {
                    Command::new("xbps-install").arg("-S").status()?;
                    Command::new("xbps-install").args(&args[2..]).status()?;
                } else {
                    print!("Nothing provided to install")
                }
            }
            "remove" | "rm" => {
                elevate::escalate_if_needed()?;
                if args.len() > 2 {
                    Command::new("xbps-remove").args(&args[2..]).status()?;
                } else {
                    print!("Nothing provided to remove")
                }
            }
            "hold" | "ho" => {
                Command::new("xbps-query").arg("--list-hold-pkg").status()?;
            }
            "hold+" | "ho+" => {
                elevate::escalate_if_needed()?;
                if args.len() > 2 {
                    Command::new("xbps-pkgdb")
                        .arg("-v")
                        .arg("--mode")
                        .arg("hold")
                        .args(&args[2..])
                        .status()?;
                    Command::new("xbps-query").arg("--list-hold-pkg").status()?;
                } else {
                    print!("Nothing provided to hold")
                }
            }
            "hold-" | "ho-" => {
                elevate::escalate_if_needed()?;
                if args.len() > 2 {
                    Command::new("xbps-pkgdb")
                        .arg("-v")
                        .arg("--mode")
                        .arg("unhold")
                        .args(&args[2..])
                        .status()?;
                    Command::new("xbps-query").arg("--list-hold-pkg").status()?;
                } else {
                    print!("Nothing provided to unhold")
                }
            }
            "find" | "fi" => {
                if args.len() > 2 {
                    Command::new("xbps-query").arg("-f").arg(&args[2]);
                } else {
                    print!("Nothing provided to find")
                }
            }
            "local" | "lo" => {
                if args.len() > 2 {
                    let query = Command::new("xbps-query")
                        .arg("-l")
                        .spawn()?
                        .stdout
                        .unwrap();
                    Command::new("grep")
                        .arg(&args[2])
                        .stdin(Stdio::from(query))
                        .status()?;
                } else {
                    let query = Command::new("xbps-query")
                        .arg("-l")
                        .spawn()?
                        .stdout
                        .unwrap();
                    Command::new("less").stdin(Stdio::from(query)).status()?;
                }
            }
            "update" | "up" => {
                elevate::escalate_if_needed()?;
                Command::new("xbps-install").arg("-Suv").status()?;
            }
            "update+" | "" | "up+" => {
                elevate::escalate_if_needed()?;
                Command::new("xbps-install").arg("-Suv").status()?;
                println!("Clear Cache...");
                Command::new("xbps-remove").arg("-Ov").status()?;
                println!("Clear Orphans...");
                Command::new("xbps-remove").arg("-ov").status()?;
            }
            "cache" | "ca" => {
                Command::new("xbps-remove").arg("-Ov").status()?;
            }
            "orphans" | "or" => {
                elevate::escalate_if_needed()?;
                Command::new("xbps-remove").arg("-ov").status()?;
            }
            "clean" | "cl" => {
                elevate::escalate_if_needed()?;
                Command::new("xbps-remove").arg("-Oov").status()?;
            }
            "repos" | "re" => {
                Command::new("xbps-query").arg("-L").status()?;
            }
            "sync" | "sy" => {
                Command::new("xbps-install").arg("-S").status()?;
            }
            "help" | _ => {
                println!("Usage: voidsent [OPERATION] [PKGNAME...]");
                println!("");
                println!("OPERATIONS");
                println!("help       (he)    Displays this help message");
                println!("install    (in)    Sync repository index, install available PKGs");
                println!("                   Unavailable PKGNAMEs will be skipped and listed");
                println!("remove     (rm)    Remove PKGNAMEs from the system");
                println!("search     (se)    Search for PKGNAME in the remote repositories");
                println!("                   An install option for the listed PKGs is porvided");
                println!("hold       (ho)    List packages on hold state");
                println!("hold+      (ho+)   Hold PKGNAMEs");
                println!("hold-      (ho-)   Unhold PKGNAMEs");
                println!("find       (fi)    Show package files for PKGNAME");
                println!("info               Show information about PKGNAME");
                println!("local      (lo)    List installed packages page-wis");
                println!("                   Enter PKGNAME to list installed packages for PKGNAME");
                println!("update     (up)    Sync repository index and update system");
                println!("update+    (up+)   Sync repository index, update system, clean cache,");
                println!("                   and remove orphans");
                println!("cache      (ca)    Clean cache");
                println!("orphans    (or)    Remove orphans");
                println!("clean      (cl)    Clean cache and remove orphans");
                println!("repos      (re)    List registered repositories");
                println!("sync       (sy)    Sync repository index");
            }
        }
    } else {
        elevate::escalate_if_needed()?;
        Command::new("xbps-install").arg("-Suv").status()?;
        println!("Clear Cache...");
        Command::new("xbps-remove").arg("-Ov").status()?;
        println!("Clear Orphans...");
        Command::new("xbps-remove").arg("-ov").status()?;
    }
    Ok(())
}
