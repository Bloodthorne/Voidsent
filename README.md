# Voidsent

Void Linux Package Installation and Management Tool

Based on [VPK](https://gitlab.com/Irkiosan/voidlinux-xbps-wrapper) - with an improved, relevance sorted, search.

---
```
Usage: voidsent [OPERATION] [PKGNAME...]

OPERATIONS
help       (he)    Displays this help message
install    (in)    Sync repository index, install available PKGs
                   Unavailable PKGNAMEs will be skipped and listed
remove     (rm)    Remove PKGNAMEs from the system
search     (se)    Search for PKGNAME in the remote repositories
                   An install option for the listed PKGs is porvided
hold       (ho)    List packages on hold state
hold+      (ho+)   Hold PKGNAMEs
hold-      (ho-)   Unhold PKGNAMEs
find       (fi)    Show package files for PKGNAME
info               Show information about PKGNAME
local      (lo)    List installed packages page-wis
                   Enter PKGNAME to list installed packages for PKGNAME
update     (up)    Sync repository index and update system
update+    (up+)   Sync repository index, update system, clean cache,
                   and remove orphans
cache      (ca)    Clean cache
orphans    (or)    Remove orphans
clean      (cl)    Clean cache and remove orphans
repos      (re)    List registered repositories
sync       (sy)    Sync repository index
```